export const getDataPoints = (step, max) => {
    const elements = [];
    if(step <=0) return elements;
    for (var i = 0; i * step <= max; i++) {
        elements.push({
            name: i * step + " PI",
            x: i * Math.PI * step
        });
    }
    return elements;
};

export const addEntry = (data, name, calc) => {
    return data.map(
        entry => {
            return {...entry, [name]: calc(entry.x)};
        }
    );
};

export const factorialize = (num) => {
    var result = num;
    if (num === 0 || num === 1)
        return 1;
    while (num > 1) {
        num--;
        result *= num;
    }
    return result;
};

export const randomColor = () =>
    '#' + Math.floor(Math.random() * 16777215).toString(16);
