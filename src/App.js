import React, {Component} from 'react';
import './App.css';
import {CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis} from 'recharts';
import {addEntry, factorialize, getDataPoints, randomColor} from "./calculations";

class App extends Component {
    state = {
        step: 0.1,
        max: 2,
        visible: [4,5,6],
        calcFunctions: [
            calc1, calc2, calc3, calc4, calc5, calc6
        ]
    };

    render() {
        let data = this.generateData();

        return (
            <div className="App">
                {this.renderForm()}
                {this.renderSeriesEnabler()}
                {this.renderChart(data)}
                {this.renderSummary(data)}
            </div>
        );
    }

    renderChart = (data) => {
        return (
            <LineChart width={1000} height={600} data={data} margin={{top: 5, right: 20, bottom: 5, left: 0}}>
                <Line type="linear" dataKey='sin' dot={false} stroke={randomColor()}/>
                {this.renderLines()}
                <CartesianGrid stroke="#ccc"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Legend/>
                <Tooltip/>
            </LineChart>
        );
    };

    renderLines = () => {
        return this.state.visible
            .map(i => <Line type="linear" dot={false} dataKey={`calc${i}`} stroke={randomColor()}/>);
    };

    generateData = () => {
        let data = getDataPoints(this.state.step, this.state.max);
        data = addEntry(data, "sin", (x) => Math.sin(x));
        const calcFunctions = this.state.calcFunctions;
        for (let i = 0; i < calcFunctions.length; i++) {
            data = addEntry(data, `calc${i + 1}`, calcFunctions[i]);
        }
        console.log(data);
        return data;
    };

    renderForm = () => {
        return (<div>
            <label>step</label>
            <input type="text" name="step" value={this.state.step} onChange={this.handleChange}/>
            <label>count</label>
            <input type="text" name="max" value={this.state.max} onChange={this.handleChange}/>
        </div>);
    };

    renderSeriesEnabler = () => {
        const elements = [];
        for (var i = 0; i < this.state.calcFunctions.length; i++) {
            elements.push(<div><input type="checkbox" value={i + 1}
                                      checked={this.state.visible.includes(i + 1)}
                                      name={i + 1} onChange={this.toggle}/>calc{i + 1}</div>);
        }
        return elements;
    };

    renderSummary = (data) => {
        let stats = {
            calc1: 0,
            calc2: 0,
            calc3: 0,
            calc4: 0,
            calc5: 0,
            calc6: 0
        };
        for (let entry of data) {
            stats.calc1 = stats.calc1 + Math.abs(entry.sin - entry.calc1);
            stats.calc2 = stats.calc2 + Math.abs(entry.sin - entry.calc2);
            stats.calc3 = stats.calc3 + Math.abs(entry.sin - entry.calc3);
            stats.calc4 = stats.calc4 + Math.abs(entry.sin - entry.calc4);
            stats.calc5 = stats.calc5 + Math.abs(entry.sin - entry.calc5);
            stats.calc6 = stats.calc6 + Math.abs(entry.sin - entry.calc6);
        }
        return (<div>
                AverageDeviations:<br/>
                calc1: {stats.calc1 / data.length}<br/>
                calc2: {stats.calc2 / data.length}<br/>
                calc3: {stats.calc3 / data.length}<br/>
                calc4: {stats.calc4 / data.length}<br/>
                calc5: {stats.calc5 / data.length}<br/>
                calc6: {stats.calc6 / data.length}
            </div>
        );
    };

    handleChange = (event) => {
        const {target} = event;
        this.setState({[target.name]: target.value});
    };

    toggle = (event) => {
        const {target} = event;
        const id = parseInt(target.name);
        if (this.state.visible.includes(id)) {
            this.setState(prevState => {
                const visible = [...prevState.visible];
                return {
                    visible: visible.filter(n => n !== id)
                };
            });
        } else {
            this.setState(prevState => {
                return {
                    visible: [...prevState.visible, id]
                };
            });
        }
    };
}

const calc1 = (x) => x - (Math.pow(x, 3) / factorialize(3));
const calc2 = (x) => calc1(x) + Math.pow(x, 5) / factorialize(5);
const calc3 = (x) => calc2(x) - Math.pow(x, 7) / factorialize(7);
const calc4 = (x) => calcOnMod(calc1)(x);
const calc5 = (x) => calcOnMod(calc2)(x);
const calc6 = (x) => calcOnMod(calc3)(x);

const calcOnMod = (f) => (x) => {
    const modulo = x % (Math.PI * 2);
    if (modulo < Math.PI / 2) {
        return f(modulo);
    }
    if (modulo < Math.PI) {
        return f(Math.PI - modulo);
    }
    return calcOnMod(f)(x - Math.PI) * -1;
};
export default App;
